$('.slider-principal').slick({
    dots: true,
    infinite: true,
    speed: 1200,
    slidesToShow: 1,
    adaptiveHeight: true,
    autoplay: true,
    autoplaySpeed: 10000,
  });

  $("div.menu > li > a").on('click', function(event){
    event.preventDefault();
    $("html, body").animate({scrollTop:$(this.hash).offset().top}, 800);
  })